import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig(
  {
    build: {
    outDir: 'public', // Specify your desired output directory
  },
plugins: [react()],
})
